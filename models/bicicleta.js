var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            console.log(aBiciId);
            return Bicicleta.allBicis.splice(i, 1);
            
        }
    
    }
    throw new Error(`No Bicicleta to delete with id ${aBiciId}`);
    //console.log(this.allBicis);
}

var a = new Bicicleta (1, 'rojo', 'urbana', [-12.0557, -76.9360]);
var b = new Bicicleta (2, 'verde', 'urbana', [-12.5557, -76.4360]);

Bicicleta.add(a);
Bicicleta.add(b);


//falla el boton no elimina de la lista
module.exports = Bicicleta;