var map = L.map('main_map').setView([-12.0557, -76.9360], 8);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([-11.0557, -66.9360]).addTo(map);
L.marker([-12.0557, -66.9360]).addTo(map);
L.marker([-12.0557, -76.9360]).addTo(map);
